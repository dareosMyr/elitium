# README

Elitium is my version of the Odin Project Ruby on Rails course project "members-only".
The purpose of said project is to create from scratch my own user authentication system for a simple, message-board-like website. Bcrypt is used for hashing user passwords.

The website allows users to sign in, view posts and make new ones. Website displays post authors only if a user is logged in. Users are identified by a token which is regenerated and encrypted on each login and stored as a permanent cookie. 

Rails ActiveRecord is used to create relationships between posts and their authors (users).

Styling was not the purpose of this project, therefore the project includes very light CSS work.