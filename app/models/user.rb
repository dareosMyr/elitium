class User < ApplicationRecord
  has_many :posts
  before_create :create_token
  validates :username, :email, presence: true, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6, maximum: 20 }

 def create_token
  if self.remember_digest.nil?
    self.remember_digest = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
  else
    update_attribute(:remember_digest, Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64))
  end
 end

end
