module SessionsHelper

  def sign_in(user)
    user.create_token
    cookies.permanent[:token_digest] = user.remember_digest
  end

  def get_user
    if cookies[:token_digest]
      @current_user ||= User.find_by(remember_digest: cookies[:token_digest])
    end
  end

  def sign_out
    cookies.delete(:token_digest)
    redirect_to root_path
  end

  def logged_in?
    !get_user.nil?
  end
end
