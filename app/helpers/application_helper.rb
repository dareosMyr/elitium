module ApplicationHelper
  def title(page_title = '')
    base_title = 'Elitium'
    base_title + ' | ' + page_title
  end
end
