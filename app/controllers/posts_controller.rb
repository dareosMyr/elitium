class PostsController < ApplicationController
  before_action :logged_in?

  def new
  end

  def create
    post = Post.new(title: params[:post][:title], body: params[:post][:body], user_id: @current_user.id)
    if post.save
      flash[:success] = 'Post created!'
      redirect_to posts_path
    else
      flash.now[:error] = 'Invalid post fields'
      render 'new'
    end
  end

  def index
    @posts = Post.all.reverse
  end

  private

    def post_params
      params.require(:post).permit(:title, :body)
    end
end
